package com.via.interview.book;

import com.via.interview.book.model.Book;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends PagingAndSortingRepository<Book, Integer> {
  List<Book> findAll();
}
