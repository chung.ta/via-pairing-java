package com.via.interview.book.controller;

import com.via.interview.book.BookRepository;
import com.via.interview.book.model.Book;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.util.List;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@AllArgsConstructor
@RestController
public class BookController {

  // Why is this not null?
  private BookRepository bookRepository;

  @Operation(
      description = "Get book by id",
      operationId = "getBookById",
      tags = "Book",
      responses =
          @ApiResponse(
              responseCode = "200",
              content = @Content(schema = @Schema(implementation = Book.class))))
  @GetMapping(path = "/{id}")
  public Book getBookById(@PathVariable String id) {
    // Basic input validation
    // How do we avoid this validation?
    // What exception this we throw?
    if (NumberUtils.isDigits(id)) {}

    // How do we avoid this type conversion?
    var book = bookRepository.findById(Integer.valueOf(id));

    return book.orElseThrow(
        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Book now found"));
  }

  @Operation(
      description = "Get all books",
      operationId = "getBooks",
      tags = "Book",
      responses =
          @ApiResponse(
              responseCode = "200",
              content = @Content(schema = @Schema(implementation = Book.class))))
  @GetMapping(path = "/")
  public List<Book> getBooks() {

    return bookRepository.findAll();
  }

  @Operation(
      description = "Save book",
      operationId = "saveBook",
      tags = "Book",
      responses =
          @ApiResponse(
              responseCode = "200",
              content = @Content(schema = @Schema(implementation = Book.class))))
  @PostMapping
  public Book saveBook(@RequestBody Book book) {

    return bookRepository.save(book);
  }
}
