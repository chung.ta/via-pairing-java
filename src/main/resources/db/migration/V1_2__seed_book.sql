INSERT INTO book(title, author)
VALUES
    ('Pride and Prejudice', 'Jane Austen'),
    ('1984', 'George Orwell'),
    ('The Odyssey', 'Homer'),
    ('Great Expectations', 'Charles Dickens');