package com.via.interview.book;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.via.interview.book.controller.BookController;
import com.via.interview.book.model.Book;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@AutoConfigureMockMvc
@WebMvcTest(controllers = BookController.class)
public class BookControllerIT {

  @Autowired private MockMvc mockMvc;

  @MockBean private BookRepository bookRepository;

  @Captor ArgumentCaptor<Book> bookArgumentCaptor;

  ObjectMapper objectMapper = new ObjectMapper();

  @Test
  public void getBookById_shouldReturnBook() throws Exception {
    // Given
    var book = new Book();
    book.setTitle("test");
    book.setAuthor("authTest");
    when(bookRepository.findById(1)).thenReturn(Optional.of(book));

    // Then
    mockMvc
        .perform(MockMvcRequestBuilders.get("/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.title").value("test"))
        .andExpect(jsonPath("$.author").value("authTest"));
  }

  @Test
  public void shouldSaveBook() throws Exception {
    // Given
    var book = new Book();
    book.setTitle("test");
    book.setAuthor("authTest");
    when(bookRepository.save(book)).thenReturn(book);

    // When
    mockMvc
        .perform(
            MockMvcRequestBuilders.post("/")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(book)))
        .andExpect(status().isOk());

    // Then
    verify(bookRepository).save(bookArgumentCaptor.capture());
    var savedBook = bookArgumentCaptor.getValue();
    assertEquals(savedBook.getAuthor(), "authTest");
    assertEquals(savedBook.getTitle(), "test");
  }
}
