package com.via.interview.book


import com.via.interview.book.controller.BookController
import com.via.interview.book.model.Book
import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@AutoConfigureMockMvc
@WebMvcTest(controllers = BookController.class)
class BookControllerITSpec extends Specification {

    @Autowired
    private MockMvc mockMvc
    @SpringBean private BookRepository bookRepository = Mock()

    def "Should return a book with the specified id"() {
        given:
        bookRepository.findById(1) >> Optional.of(new Book(id:1, title: "test", author: "authTest"))

        expect:
        mockMvc.perform(get("/1")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath('$.title').value("test"))
                .andExpect(jsonPath('$.author').value("authTest"))
    }
}
