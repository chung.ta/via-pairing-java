# Via's interactive coding session

Welcome to the coding session of the interview! The purpose of this session is for us to pair together on solving some straightforward problems that mimic things that might come up during an actual work day. We don't want to see you solve some arbitrary riddle - we want to get a feel for how we might work together.

## Repo summary

This repo includes code that sets up a basic API for returning information about books. It is a Spring Boot app with an embedded H2 database.

## List of framework/platform/dependent components included.

* Language: [Java v17]
* Framework: [Spring Boot v2.7.5]
* Other: [Spring Data JPA], [Spring Data JDBC], [Lombok], [Flyway], [Spock], [Spring Doc]
* Databases: [H2]

To get started:

```
./gradlew check
./gradlew bootRun
```

[Spring doc](http://localhost:8080/docs/swagger-ui/index.html)

## Tasks

There are multiple tasks that we want to implement in this existing repo. Feel free to start with any task that you'd like.

1. We currently only have a way to track a book's author via the `author` field on the `book` entity. We've been asked to return additional details about authors, including nickname, birth place, and birth date.
2. We've been asked to provide an endpoint for getting all books, with the ability to filter based on title and author.
3. We've been asked to add `genre` functionality. We need the ability to tag a book with potentially multiple genres. A book's genres should be included in an array from any endpoints that return book information.
4. We need to add a new endpoint that allows the caller to create a new `book`.

## Some helpful commands

### Running tests

This repo uses `gradle`

```
./gradlew test
```
